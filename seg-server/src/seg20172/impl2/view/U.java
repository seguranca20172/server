package seg20172.impl2.view;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class U {

	private static final Scanner IN = new Scanner(System.in);

	public static void say(final String prompt, final Object... args) {
		System.out.println(String.format(prompt, args));
	}

	public static String ask(final String prompt, final Object... args) {
		say(prompt, args);
		System.out.print("> ");
		String line;
		do {
			line = IN.nextLine();
		} while(StringUtils.isBlank(line));
		return line;
	}

}
