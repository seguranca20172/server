package seg20172.impl2.security;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v1CertificateBuilder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

public class CertificateUtils {

	private static final long THIRTY_DAYS = 1000L * 60 * 60 * 24 * 30;

	private static X509Certificate makeV1Certificate(final PrivateKey caSignerKey, final PublicKey caPublicKey)
		throws GeneralSecurityException, IOException, OperatorCreationException {
		final X509v1CertificateBuilder v1CertBldr = new JcaX509v1CertificateBuilder(
			new X500Name("CN=Issuer CA"),
			BigInteger.valueOf(System.currentTimeMillis()),
			new Date(System.currentTimeMillis() - 1000L * 5),
			new Date(System.currentTimeMillis() + THIRTY_DAYS),
			new X500Name("CN=Issuer CA"),
			caPublicKey);

		final JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA256WithRSAEncryption").setProvider("BCFIPS");
		return new JcaX509CertificateConverter().setProvider("BCFIPS").getCertificate(v1CertBldr.build(signerBuilder.build(caSignerKey)));
	}

	private static X509Certificate makeV3Certificate(final X509Certificate caCertificate, final PrivateKey caPrivateKey, final PublicKey eePublicKey)
		throws GeneralSecurityException, CertIOException, OperatorCreationException {
		final X509v3CertificateBuilder v3CertBldr = new JcaX509v3CertificateBuilder(
			caCertificate.getSubjectX500Principal(),
			BigInteger.valueOf(System.currentTimeMillis()).multiply(BigInteger.valueOf(10)),
			new Date(System.currentTimeMillis() - 1000L * 5),
			new Date(System.currentTimeMillis() + THIRTY_DAYS),
			new X500Principal("CN=Cert V3 Example"), eePublicKey);

		final JcaX509ExtensionUtils extUtils = new JcaX509ExtensionUtils();
		v3CertBldr.addExtension(Extension.subjectKeyIdentifier, false, extUtils.createSubjectKeyIdentifier(eePublicKey));
		v3CertBldr.addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(caCertificate.getPublicKey()));
		v3CertBldr.addExtension(Extension.basicConstraints, true, new BasicConstraints(false));

		final JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA256WithRSAEncryption").setProvider("BCFIPS");
		return new JcaX509CertificateConverter().setProvider("BCFIPS").getCertificate(v3CertBldr.build(signerBuilder.build(caPrivateKey)));
	}

	public static X509Certificate makeSignedCertificate(final PublicKey publicKey, final PublicKey caPublicKey, final PrivateKey caPrivateKey)
		throws OperatorCreationException, GeneralSecurityException, IOException {
		final X509Certificate caCertificate = makeV1Certificate(caPrivateKey, caPublicKey);
		return makeV3Certificate(caCertificate, caPrivateKey, publicKey);
	}

}
