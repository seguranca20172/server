package seg20172.impl2.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class KeyStoreManager {

	private final File file;

	private final KeyStore ks;
	private final String password;
	private final String pkPassword;

	public KeyStoreManager(final File file, final String password, final String pkPassword)
		throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, NoSuchProviderException {
		ks = KeyStore.getInstance("PKCS12");
		if(file.exists()) {
			ks.load(new FileInputStream(file), password.toCharArray());
		} else {
			ks.load(null, null);
			ks.store(new FileOutputStream(file), password.toCharArray());
		}
		this.file = file;
		this.password = password;
		this.pkPassword = pkPassword;
	}

	public X509Certificate getCertificate() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
		return (X509Certificate) ks.getCertificate("cert");
	}

	public void setCertificate(final Certificate certificate) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
		ks.setCertificateEntry("cert", certificate);
		ks.store(new FileOutputStream(file), password.toCharArray());
	}

	public PrivateKey getPrivateKey() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
		return (PrivateKey) ks.getKey("pk", pkPassword.toCharArray());
	}

	public void setPrivateKey(final PrivateKey privateKey, final X509Certificate certificate)
		throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
		ks.setKeyEntry("pk", privateKey, pkPassword.toCharArray(), new X509Certificate[] { certificate });
		ks.store(new FileOutputStream(file), password.toCharArray());
	}

}
