package seg20172.impl2.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.fips.FipsDRBG;
import org.bouncycastle.crypto.util.BasicEntropySourceProvider;
import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.bouncycastle.operator.OperatorCreationException;

import seg20172.impl2.security.CertificateUtils;
import seg20172.impl2.security.KeyStoreManager;
import seg20172.impl2.security.crypto.AESCipher;
import seg20172.impl2.security.crypto.RSACipher;
import seg20172.impl2.security.random.FixedRandom;
import seg20172.impl2.view.U;

public class Server {

	static {
		Security.addProvider(new BouncyCastleFipsProvider());
		CryptoServicesRegistrar.setSecureRandom(FipsDRBG.SHA512_HMAC.fromEntropySource(new BasicEntropySourceProvider(new SecureRandom(), true)).build(null, false));
	}

	public static void main(final String[] args) throws IOException, DecoderException, OperatorCreationException, GeneralSecurityException {
		final Server server = new Server(45678);
		server.listen();
		server.shutdown();
	}

	private final KeyStoreManager keyStoreManager;

	private final AESCipher aes;
	private final RSACipher rsa;

	private final ServerSocket server;

	private X509Certificate cert;

	private Socket client;
	private PrintStream out;
	private Scanner in;

	public Server(final int port) throws IOException, OperatorCreationException, GeneralSecurityException {
		final SecureRandom random = new FixedRandom();

		aes = new AESCipher(random);
		rsa = new RSACipher(random);

		keyStoreManager = new KeyStoreManager(
			new File("ks.p12"),
			U.ask("Senha da KeyStore"),
			U.ask("Senha da chave privada"));

		PublicKey publicKey = null;
		PrivateKey privateKey = null;
		try {

			cert = keyStoreManager.getCertificate();
			privateKey = keyStoreManager.getPrivateKey();
			if(cert == null || privateKey == null) {
				throw new UnrecoverableKeyException();
			}

			publicKey = cert.getPublicKey();
		} catch(final UnrecoverableKeyException e) {
			final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", "BCFIPS");

			final KeyPair caKeyPair = keyPairGenerator.generateKeyPair(), keyPair = keyPairGenerator.generateKeyPair();

			cert = CertificateUtils.makeSignedCertificate(keyPair.getPublic(), caKeyPair.getPublic(), caKeyPair.getPrivate());

			publicKey = cert.getPublicKey();
			privateKey = keyPair.getPrivate();

			keyStoreManager.setCertificate(cert);
			keyStoreManager.setPrivateKey(privateKey, cert);
		} finally {
			rsa.setPublicKey(publicKey);
			rsa.setPrivateKey(privateKey);
		}

		server = new ServerSocket(port);
	}

	public void listen() throws IOException, DecoderException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException,
		KeyStoreException, NoSuchAlgorithmException, CertificateException {
		U.say("Aguardando conexao...");
		client = server.accept();
		U.say("Cliente conectado!");

		in = new Scanner(client.getInputStream());
		out = new PrintStream(client.getOutputStream());

		final char[] certificate = Hex.encodeHex(cert.getEncoded());
		U.say("Enviando certificado:\n%s", String.valueOf(certificate));
		out.println(certificate);

		U.say("Aguardando chave simetrica...");
		while(!in.hasNextLine()) {}

		final String simmetricKey = in.nextLine();
		U.say("Recebida chave simetrica:\n%s", simmetricKey);

		final byte[] encodedKey = Hex.decodeHex(simmetricKey.toCharArray());
		final SecretKeySpec key = new SecretKeySpec(rsa.decrypt(encodedKey), "AES");
		aes.setKey(key);

		while(in.hasNextLine()) {
			final String received = in.nextLine();
			final String decrypted = new String(aes.decrypt(Hex.decodeHex(received.toCharArray())));
			U.say(
				"\n---------------------------------------------------------------------------------\nMENSAGEM RECEBIDA\nMensagem texto plano\t>>>>\t%s\nMensagem codificada\t>>>>\t%s\n---------------------------------------------------------------------------------\n",
				decrypted,
				received);

			final String message = U.ask("Mensagem para o cliente");

			final char[] sent = Hex.encodeHex(aes.encrypt(message.getBytes()));
			U.say(
				"\n---------------------------------------------------------------------------------\nMENSAGEM ENVIADA\nMensagem texto plano\t>>>>\t%s\nMensagem codificada\t>>>>\t%s\n---------------------------------------------------------------------------------\n",
				message,
				String.valueOf(sent));

			out.println(sent);

			U.say("Aguardando resposta do cliente...");
		}
	}

	public void shutdown() throws IOException {
		server.close();

		if(client != null) {
			client.close();
		}
		if(in != null) {
			in.close();
		}
		if(out != null) {
			out.close();
		}
	}

}
